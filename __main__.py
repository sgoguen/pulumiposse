import git
import os
import pulumi
import pulumi_github as github
import subprocess

# module clone location
current_dir = os.getcwd()
tf_repos_dir = f"{current_dir}/modules"
if not os.path.exists(tf_repos_dir):
    os.mkdir(tf_repos_dir)

# get terraform repos
community_aws_org = github.get_organization(name="terraform-aws-modules")
cloudposse_org = github.get_organization(name="cloudposse")
all_repos = community_aws_org.repositories + cloudposse_org.repositories
repos = [repo for repo in all_repos if "/terraform" in repo]
for repo in repos:
    tf_repo_dir = f"{tf_repos_dir}/{repo}"
    if not os.path.exists(tf_repo_dir):
        repo_clone_url = f"https://github.com/{repo}"
        print(f"{tf_repo_dir}")
        repo = git.Repo.clone_from(repo_clone_url, tf_repo_dir)
        #p = subprocess.Popen(["terraform", "init"], cwd=tf_repo_dir)
        # p.wait()
