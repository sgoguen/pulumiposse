#!/bin/bash
tfdir=$1
modname=$(basename $tfdir)
moddir=$(dirname $tfdir|sed 's|.*modules/||')
tfdesc="$moddir/$modname 2 Pulumi"
stack="dev"

pushd $tfdir
terraform init
pulumi new python --name $modname --description "$tfdesc" --stack "$stack" --force --non-interactive
source venv/bin/activate
pip install -r requirements.txt >/dev/null
set -x
tf2pulumi --target-language python --allow-missing-plugins --record-locations --allow-missing-variables
pulumi destroy --yes
pulumi stack rm $stack --yes
rm -rf .terraform  #.git
